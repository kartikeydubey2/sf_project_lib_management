public class BorrowRequestTriggerHelper
{
    public static List<Borrow_Request__c> validateInsert(List<Borrow_Request__c> borrowRequests)
    {
          Set<ID> itemIDs = new Set<ID>();
          Set<ID> employeeIDs = new Set<ID>();
          
        for(Borrow_Request__c borrow:borrowRequests)
            
        {
            itemIDs.add(borrow.Items__c);
            employeeIDs.add(borrow.Employess__c);
        }
     Map<Id,Library_item__c> libItems=new Map<Id,Library_item__c>([Select id,Quantity__c,total_active_borrow_request__c,Availablities__c from  Library_item__c where id in: itemIDs]) ; 
     Map<Id,Employee__c>  empRec  =new Map<Id,Employee__c>([Select Id,Employee_Email__c from Employee__c Where Id in: employeeIDs]);
     
        List<Borrow_Request__c> addError =new List<Borrow_Request__c>();
        
        if(!libItems.isEmpty())
        {
          for(Borrow_Request__c borrow:borrowRequests)
          {
              if(libItems.containsKey(borrow.Items__c) && libItems.get(borrow.Items__c).Availablities__c<=0 && borrow.Is_Active__c==true)
              {
                  addError.add(borrow);
              }
              borrow.Employess_Email__c = empRec.containsKey(borrow.Employess__c) ? empRec.get(borrow.Employess__c).Employee_Email__c:''; 
          }
        }
        return addError;
    }
}