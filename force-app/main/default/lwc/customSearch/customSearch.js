import { LightningElement, track ,wire } from 'lwc';

import getItems from '@salesforce/apex/LwcController.getItems';
import getLibItems from '@salesforce/apex/LwcController.getLibItems';


export default class CustomSearch extends LightningElement {
    @track items;
     key='';
    @track cols = [{
        label: 'Library item Name',
        fieldName: 'Name',
        type: 'url',
        typeAttributes: {label: { fieldName: 'Name' }, target: '_blank'}
        
        		
    },
    {
        label: 'Availablity',
        fieldName: 'Availablities__c',
        type: 'Number',
    },
    {
        label: 'Quantity',
        fieldName: 'Quantity__c',
        type: 'Number',
    },

    {
        label: 'Item_Type',
        fieldName: 'Item_Type__c',
        type: 'Text',
    }
]

connectedCallback(){

    getItems()
    .then(results=>{
        this.items=results;
    })
    .catch(error=>{
        this.error=error;
    });
 }



    updateKey(event){

        this.key=event.target.value;
    }

    handleSearch()

    {     if(this.key !== ''){
         getLibItems({searchkey : this.key})
         .then(result=>{
             this.items=result;
         })
         .catch(error=>{

            const event =new showToastEvent(
                {
                    title:'Error',
                    variant:'error',
                    message:error.body.message,
                }
            );
             this.dispatchEvent(event);
             this.items=null;
         });

         
    } 
    else{
        const event = new showToastEvent({

            variant: 'error',
            message: 'Search text missing..',
            });
            this.dispatchEvent(event);
    }


    




    }

}