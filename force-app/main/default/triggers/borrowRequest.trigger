trigger borrowRequest on Borrow_Request__c (before insert) {
    List<Borrow_Request__c> addErrorInsertion;
    List<Borrow_Request__c> addErrorUpdation;
    if(Trigger.isInsert)
    {    
        addErrorInsertion = BorrowRequestTriggerHelper.validateInsert(Trigger.new);
    
        if(!addErrorInsertion.isEmpty())
        {
           for(Borrow_Request__c request : addErrorInsertion)
           {
               request.addError('!!! OUT OF STOCK/AVAILABILTY !!!');
           }
        }
    }
}